ARG NODE_TAG=16-alpine
FROM node:${NODE_TAG}

RUN apk add --no-cache git curl jq git-lfs
RUN curl -sSLf "$(curl -sSLf https://api.github.com/repos/tomwright/dasel/releases/latest | grep browser_download_url | grep linux_amd64 | cut -d\" -f 4)" -L -o /usr/local/bin/dasel && chmod +x /usr/local/bin/dasel
RUN npm install --cache /tmp/empty-cache -g semantic-release @semantic-release/gitlab @semantic-release/git @semantic-release/changelog @semantic-release/exec

ENTRYPOINT [""]
